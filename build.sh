#!/bin/bash -x
set -e

id
ls -la /
echo "$SHELL"

mkdir -p build
cd build

cat > index.html <<EOF
<!DOCTYPE html>
<html>
    <head>
        <title>Build</title>
    </head>
    <body>
        <h1>Build</h1>
        <p>It works folks!</p>
        <hr />
        <pre>Vojtech</pre>
    </body>
</html>
EOF
